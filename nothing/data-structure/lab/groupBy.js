/**
 * Created by hanni on 10/18/16.
 */

var array = crushing(data);

function groupBy(obj, byIdent, callback) {
    var rezult = {};
    var arrOfElement = [];
    var preRezult = [];

    obj[byIdent].forEach( function (value, i) {

        if (ver(arrOfElement, value) || i == 0) {
            rezult[byIdent] = value;
            rezult.count = callback(value, i, byIdent);
            preRezult.push(rezult);
            rezult = {};
            arrOfElement.push(value);
        }
    });

    return preRezult.sort( function (valueA, valueB) {

        if (valueA.count > valueB.count) {
            return 1;
        }
        if (valueA.count < valueB.count) {
            return -1;
        }

        return 0;
    });
}

function ver(arr, element) {
    var count = 0;

    arr.forEach( function (value) {
        if (value == element) {
            count++;
        }
    });

    return count == 1 ? false : true;
}

function unzipInObj(arr) {
    var rezultObj = {};
    var keys;

    for (var i = 1; i < arguments.length; i++){
         rezultObj[arguments[i]] = [];
     }

    keys = Object.keys(rezultObj);

    arr.forEach( function (value) {
        value.forEach( function (value1,j) {
            rezultObj[keys[j]].push(value1);
        });
    });

    return rezultObj;
}

array = unzipInObj(array, 'id', 'name', 'idprod', 'prod', 'type', 'typeid');
array = groupBy(array, 'name', function (value, i, byIdent) {

    return array[byIdent].reduce( function (count, valueRe, index) {
        return valueRe == value ? count + 1 : count;
    },0);
});

alert('Best: ' + array[array.length - 1].name + ' with:' + array[array.length - 1].count);
alert('Last: ' + array[0].name + ' with:' + array[0].count);
console.log(array);
